const {app, BrowserWindow, ipcMain, dialog, globalShortcut} = require('electron');
const fs = require("fs");

let root;

var config;

app.on('ready', createWindow);

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit();
    }
});

app.on('activate', () => {
    if (root === null) {
        createWindow();
    }
});

app.on('will-quit', function () {
    globalShortcut.unregisterAll()
})

ipcMain.on('select-directory', (e) => {
    dialog.showOpenDialog({
        properties: ['openDirectory']
    }, (files) => {
        if (files){
            e.sender.send('selected-directory', files)
        }
    })
});

ipcMain.on('config', (e) => {
    fs.readFile('config.json', (err, data) => {
        e.sender.send('config-loaded', data);
        config = JSON.parse(data);
        registrarShortcut(config.shortcut);
    });
})

ipcMain.on('saveConfig', (e, config) => {
    var temp = {
        winIni: config.winIni,
        localSave: config.localSave,
        localDir: config.localDir,
        shortcut: config.shortcut
    }
    config = temp;
    fs.writeFileSync('config.json', JSON.stringify(temp, null, 4), 'utf-8');
})


ipcMain.on('changeShortcut', (e, key) => {
    registrarShortcut(key);
});

function createWindow(){
    root = new BrowserWindow({
        width: 400,
        height: 400,
        resizable: false,
        maximized: false,
        show: false
    });
    root.loadURL("file://" + __dirname + "/index.html");
    root.setMenu(null);
    root.webContents.openDevTools();
    root.show();
}

function registrarShortcut(key){
    globalShortcut.unregisterAll();
    globalShortcut.register('CommandOrControl+Alt+' + key, function () {
        root.webContents.send('screenshot');
    })
}
