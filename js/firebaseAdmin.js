var configFir = {
    apiKey: "AIzaSyA8bY4TBuyZ0Ys3m-AjSstZIJ6XmCdF6vk",
    authDomain: "snapgame-d5cba.firebaseapp.com",
    databaseURL: "https://snapgame-d5cba.firebaseio.com",
    projectId: "snapgame-d5cba",
    storageBucket: "snapgame-d5cba.appspot.com",
    messagingSenderId: "473488314571"
};
firebase.initializeApp(configFir);

var storageRef = firebase.storage().ref();
var database = firebase.database();
var arrQueue;


function FIRlogin(){
    firebase.auth().signInWithEmailAndPassword(document.getElementById('user').value, document.getElementById('pass').value).then(function(result){
        logedUser = result;
        startListenQueue();
        loginDone();
    }).catch(function(error) {
        console.log(error.code + " - " + error.message);
    });
}

function startListenQueue(){
    var lisQueue = database.ref('queue/' + logedUser.uid);
    lisQueue.on('value', function(snapshot){
        if(snapshot.val() == null){
            arrQueue = new Array();
        }else{
            arrQueue = snapshot.val();
        }
    })
}

function uploadToQueue(ia, filename){
    var metadata = {
        contentType: 'image/png'
    };
    var blob = new Blob([ia], { type: 'image/jpeg' });
    var file = new File([blob], filename);
    storageRef.child('queue/' + file.name).put(file, metadata).then(function(snapshot){
        var today = new Date();
        arrQueue[arrQueue.length] = {
            img: filename,
            fecha: today.getDate() + "/" + today.getMonth() + "/" + today.getFullYear()
        }
        database.ref('queue/' + logedUser.uid).set(arrQueue); 
    });
}