const electron = require('electron');
const ipc = electron.ipcRenderer;
const desktopCapturer = electron.desktopCapturer;
const electronScreen = electron.screen;
const shell = electron.shell;

const fs = require('fs');
const os = require('os');
const path = require('path');

config = new Config();

window.addEventListener('load', () => {
    loadUiLogin();
})

function loginDone(){
    ipc.send('config');
    loadUiConfig();
    document.getElementById('ch-winInit').addEventListener("change", () => {
        config.winIni = document.getElementById('ch-winInit').checked;
        ipc.send('saveConfig', config);
        loadConfig();
    });
    document.getElementById('ch-saveLocal').addEventListener("change", () => {
        config.localSave = document.getElementById('ch-saveLocal').checked;
        ipc.send('saveConfig', config);
        loadConfig();
    });
    document.getElementById('txtShortcut').addEventListener("change", () => {
        config.shortcut = document.getElementById('txtShortcut').value;
        ipc.send('saveConfig', config);
        ipc.send('changeShortcut', config.shortcut);
        loadConfig();
    });
}

function openDialog(){
    ipc.send('select-directory');
}

function loadConfig(){
    document.getElementById('ch-winInit').checked = config.winIni;
    document.getElementById('ch-saveLocal').checked = config.localSave;
    document.getElementById('pathImg').value = config.localDir;
    if(!config.localSave){
        document.getElementById('pathImg').disabled = true;
        document.getElementById('browserPath').disabled = true;
    }else{
        document.getElementById('pathImg').disabled = false;
        document.getElementById('browserPath').disabled = false;
    }
    document.getElementById('txtShortcut').value = config.shortcut;
}

function determineScreenShotSize () {
    const screenSize = electronScreen.getPrimaryDisplay().workAreaSize
    const maxDimension = Math.max(screenSize.width, screenSize.height)
    return {
        width: maxDimension * window.devicePixelRatio,
        height: maxDimension * window.devicePixelRatio
    }
}

ipc.on('selected-directory', (event, imgPath) => {
    document.getElementById("pathImg").value = imgPath;
    config.localDir = document.getElementById("pathImg").value;
    ipc.send('saveConfig', config);
    loadConfig();
})

ipc.on('config-loaded', (event, json) => {
    config.setConfig(JSON.parse(json));
    loadConfig();
})

ipc.on('screenshot', (e) => {
    desktopCapturer.getSources({ types: ['screen'], thumbnailSize: determineScreenShotSize() }, function (error, sources) {
        if (error) return console.log(error)
        sources.forEach(function (source) {
            if (source.name === 'Entire screen' || source.name === 'Screen 1') {
                var screenshotPath;
                var fileName = 'SG-' + logedUser.uid + '-' + (new Date()).getTime() + '.png';
                if(config.localSave){
                    screenshotPath = path.join(config.localDir, fileName);
                    fs.writeFile(screenshotPath, source.thumbnail.toPng(), function (error) {
                        if (error) return console.log(error)
                    })
                }
                uploadToQueue(source.thumbnail.toPng(), fileName);
                let myNotification = new Notification('SnapGame', {
                    body: 'Captura de pantalla realizada con éxito!'
                })
            }
        })
    })
})

