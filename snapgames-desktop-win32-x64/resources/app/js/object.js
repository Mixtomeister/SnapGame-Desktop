function Config(){
    this.winIni = false;
    this.localSave = false;
    this.localDir = "";
    this.shortcut = "S";

    this.setConfig = (json) => {
        this.winIni = json.winIni;
        this.localSave = json.localSave;
        this.localDir = json.localDir;
        this.shortcut = json.shortcut;
    }
}