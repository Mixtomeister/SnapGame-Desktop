function loadUiLogin(){
    var h1 = document.createElement('h1');
    var section = document.createElement('section');
    var txtuser = document.createElement('input');
    var txtpass = document.createElement('input');
    var btnLogin = document.createElement('button');

    h1.innerHTML = 'SG';
    section.setAttribute('class', 'form-box');
    txtuser.setAttribute('type', 'text');
    txtuser.setAttribute('value', 'losputos@gmail.com');
    txtuser.setAttribute('id', 'user');
    txtpass.setAttribute('type', 'password');
    txtpass.setAttribute('id', 'pass');
    txtpass.setAttribute('value', '123456');
    btnLogin.innerHTML = "Login";
    btnLogin.addEventListener('click', () => {
        FIRlogin();
    });

    section.appendChild(txtuser);
    section.appendChild(txtpass);
    section.appendChild(btnLogin);
    document.body.appendChild(h1);
    document.body.appendChild(section);
}

function loadUiConfig(){
    document.body.innerHTML = "";
    var h1 = document.createElement('h1');
    var section = document.createElement('section');
    var lblWinIni = document.createElement('label');
    var chWinIni = document.createElement('input');
    var lblSaveLocal = document.createElement('label');
    var chSaveLocal = document.createElement('input');
    var div = document.createElement('div');
    var txtPath = document.createElement('input');
    var btnPath = document.createElement('button');
    var p = document.createElement('p');
    var lblAtajos = document.createElement('label');

    h1.innerHTML = 'SG';
    section.setAttribute('class', 'form-box');
    lblWinIni.htmlFor = 'ch-winInit';
    chWinIni.setAttribute('type', 'checkbox');
    chWinIni.setAttribute('id', 'ch-winInit');
    lblWinIni.appendChild(chWinIni);
    lblWinIni.innerHTML += ' Iniciar con sistema operativo';
    lblSaveLocal.htmlFor = 'ch-saveLocal';
    chSaveLocal.setAttribute('type', 'checkbox');
    chSaveLocal.setAttribute('id', 'ch-saveLocal');
    lblSaveLocal.appendChild(chSaveLocal);
    lblSaveLocal.innerHTML += ' Guardar en local';
    txtPath.setAttribute('type', 'text');
    txtPath.setAttribute('id', 'pathImg');
    txtPath.setAttribute('class', 'textfield');
    btnPath.setAttribute('id', 'browserPath');
    btnPath.addEventListener('click', () => {
        openDialog();
    });
    btnPath.innerHTML = '...';
    p.setAttribute('class', 'subti');
    p.innerHTML = 'Atajo de teclado';
    lblAtajos.innerHTML = 'Ctrl + Alt + <input type="text" maxlength="1" id="txtShortcut" />'

    div.appendChild(txtPath);
    div.appendChild(btnPath);
    section.appendChild(lblWinIni);
    section.appendChild(lblSaveLocal);
    section.appendChild(div);
    section.appendChild(p);
    section.appendChild(lblAtajos);
    document.body.appendChild(h1);
    document.body.appendChild(section);
}